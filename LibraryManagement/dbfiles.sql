create database library;

create table payments
(
 id int(10) primary key,
 userid varchar(20),
 amount int(10),
 type varchar(20),
 transaction_time varchar(25),
 nextpayment_duedate varchar(25)
);


create table users
(
 id int(10) primary key,
 name varchar(25),
 email varchar(25),
 phone varchar(15),
 passwd varchar(15),
 role varchar(15)
);


create table issuerecord
(
 id int(10) primary key,
 copyid int(10),
 memberid varchar(15),
 issue_date varchar(20),
 return_duedate date(20),
 return_date date(25),
 fine_amount int(10)
);


create table copies
(
 id int(10),
 bookid int(10),
 rack varchar(15),
 status varchar(20)
);


create table books
( 
 id int(10),
 name varchar(25),
 author varchar(25),
 subject varchar(25),
 price int(15),
 isbn varchar(20)
);

insert into books values
(
 1,
 'Harry potter',
 'J K Rowling',
 'Fiction',
 400,
 'hp1234'
);
insert into users values
(
 1,
 'owner',
 'owner@gmail.com',
 '9865985625',
 'owner',
 'owner'
);
