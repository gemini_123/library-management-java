package pojo;


public class Payments 
{
	private int id;
	private int userid;
	private double amount;
	private String type;
	private Date transaction_time;
	private Date nextpayment_duedate;
	
	public Payments(int int1, int int2, double double1, String string, String string2, String string3)
	{
		this.id = int1;
		this.userid = int2;
		this.amount = double1;
		this.type = string;
		
	}
	
	public Payments() {
		
	}

	public Payments(int id, int userid, double amount, String type, Date transaction_time, Date nextpayment_duedate) {
		this.id = id;
		this.userid = userid;
		this.amount = amount;
		this.type = type;
		this.transaction_time = transaction_time;
		this.nextpayment_duedate = nextpayment_duedate;
	}
	public Payments(int int1, int int2, double double1, String string) {
		this.id = int1;
		this.userid = int2;
		this.amount = double1;
		this.type = string;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getTransaction_time() {
		return transaction_time;
	}
	public void setTransaction_time(Date curdate) {
		this.transaction_time = curdate;
	}
	public Date getNextpayment_duedate() {
		return nextpayment_duedate;
	}
	public void setNextpayment_duedate(Date nextpayment_duedate) {
		this.nextpayment_duedate = nextpayment_duedate;
	}
	@Override
	public String toString() {
		return "Payments :id=" + id + ", userid=" + userid + ", amount=" + amount + ", type=" + type
				+ "";
	}
	

}
