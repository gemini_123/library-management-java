package pojo;

public class Copies 
{
	 private int id;
	 private int bookid ;
	 private String rack ;
	 private String status ;
	 
	public Copies() 
	{
		
	}
	public Copies(int id, int bookid, String rack, String status) 
	{
		this.id = id;
		this.bookid = bookid;
		this.rack = rack;
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getRack() {
		return rack;
	}
	public void setRack(String rack) {
		this.rack = rack;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "id: "+id+" bookid: "+bookid+" Rack: "+rack+" status: "+status;
	}
	 

}
