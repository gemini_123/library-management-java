package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import pojo.Book;
import utils.DBUtils;

public class BookDao 
{
	private Connection connection;
	private PreparedStatement stmtCount;
	private PreparedStatement stmtInsertBook;
	private PreparedStatement stmtEdit;
	private PreparedStatement stmtSearch;
	private PreparedStatement stmtShow;
	private PreparedStatement stmtShowBookAvail;
	public BookDao() 
	{
		try
		{
			this.connection=DBUtils.getConnection();
			this.stmtCount=this.connection.prepareStatement("select count(*) from books");
			this.stmtInsertBook=this.connection.prepareStatement("insert into books values (?,?,?,?,?,?)");
			this.stmtEdit=this.connection.prepareStatement("update books set name=?,author=?,subject=?,price=?,isbn=? where id=?");
			this.stmtSearch=this.connection.prepareStatement(" select * from books where name like ?");
			this.stmtShowBookAvail=this.connection.prepareStatement("select * from books");
			this.stmtShow=this.connection.prepareStatement("select b.id , b.name,b.author,b.subject, b.price,b.isbn,c.rack,c.status from  books b, copies c where c.bookid=b.id;");
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	//for adding book
	public void addBook(Book book) throws SQLException
	{
		ResultSet rs=stmtCount.executeQuery();
		int count;
		while(rs.next())
		{
		count =rs.getInt(1);
		count ++;
		this.stmtInsertBook.setInt(1, count);
		this.stmtInsertBook.setString(2, book.getName());
		this.stmtInsertBook.setString(3, book.getAuthor());
		this.stmtInsertBook.setString(4,book.getSubject());
		this.stmtInsertBook.setDouble(5,book.getPrice());
		this.stmtInsertBook.setString(6, book.getIsbn());
		this.stmtInsertBook.executeUpdate();
		System.out.println("Book Addition Successfull");
		}
	
	}
	//edit book details
	public int editBook(int id,String name,String author,String subject,double price,String isbn) throws SQLException
	{

		this.stmtEdit.setInt(6, id);
		this.stmtEdit.setString(1, name);
		this.stmtEdit.setString(2, author);
		this.stmtEdit.setString(3, subject);
		this.stmtEdit.setDouble(4, price);
		this.stmtEdit.setString(5, isbn);
		return this.stmtEdit.executeUpdate();
	}
	
	//search book by string
	public List<Book> findBook(String str) throws SQLException
	{
		 List<Book> booklist=new ArrayList<Book>();
		stmtSearch.setString(1, "%" +str+ "%");
		ResultSet rs=stmtSearch.executeQuery();
		while(rs.next())
		{
			Book book=new Book(rs.getInt("id"),rs.getString("name"),rs.getString("author"),rs.getString("subject"),rs.getDouble("price"),rs.getString("isbn"));
			booklist.add(book);
		}
		return booklist;
		
	}
	//book report
	public void bookReport() throws SQLException
	{
		// List<Payments> paymentlist=new ArrayList<Payments>();
		
		ResultSet rs=stmtShow.executeQuery();

		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" , "+rs.getString(2)+" , "+rs.getString(3)+" , "+rs.getString(4)+" , "+rs.getDouble(5)+" , "+rs.getString(6)+" , "+rs.getString(7)+" , "+rs.getString(8));
			System.out.println();
		}
		
	}
	public void bookAvailReport() throws SQLException
	{
		ResultSet rs=stmtShowBookAvail.executeQuery();

		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" , "+rs.getString(2)+" , "+rs.getString(3)+" , "+rs.getString(4)+" , "+rs.getDouble(5)+" , "+rs.getString(6));
			System.out.println();
		}
		
	}
	public void close() throws SQLException 
	{
		this.stmtCount.close();;
		this.stmtInsertBook.close();;
		this.stmtEdit.close();;
		this.stmtSearch.close();;
		this.stmtShow.close();
		this.stmtShowBookAvail.close();
		this.connection.close();
	}
}
