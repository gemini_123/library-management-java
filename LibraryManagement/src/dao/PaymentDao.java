package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import pojo.Payments;
import utils.DBUtils;

public class PaymentDao
{
	private Connection connection;
	private PreparedStatement stmtCount;
	private PreparedStatement stmtadd;
	private PreparedStatement stmtCheck;
	private PreparedStatement stmtUpdate;
	private PreparedStatement stmtShow;
	
	public PaymentDao()
	{
		try
		{
			this.connection=DBUtils.getConnection();
			this.stmtCount=this.connection.prepareStatement("select count(*) from payments");
			this.stmtadd=this.connection.prepareStatement("insert into payments values (?,?,?,?,?,?)");
			this.stmtCheck=this.connection.prepareStatement("select * from payments where amount>0 and type='membership' and userid=?");
			this.stmtUpdate=this.connection.prepareStatement("insert into payments values (?,?,?,?,?,?)");
			this.stmtShow=this.connection.prepareStatement("select * from payments");
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public int takePAyment(Payments payment) throws SQLException
	{
		ResultSet rs=stmtCount.executeQuery();
		int count,flag = 0;
		while(rs.next())
		{
		count =rs.getInt(1);
		count ++;
		this.stmtadd.setInt(1, count);
		//System.out.println(count);
		this.stmtadd.setInt(2, payment.getUserid());
		this.stmtadd.setDouble(3, payment.getAmount());
		this.stmtadd.setString(4,payment.getType());
		this.stmtadd.setString(5, (payment.getTransaction_time().toString()));
		this.stmtadd.setString(6, (payment.getNextpayment_duedate().toString()));
		flag=this.stmtadd.executeUpdate();
		
		}
		if(flag==1)
		{
			System.out.println("Payment record Added Successfully");
			return flag;
		}
		else
		{
			System.out.println("User is not paid member");
			return flag;
		}
		
	}
	public int isPaid(int userid) throws SQLException
	{
			int flag=0;
			//List<Book> booklist=new ArrayList<Book>();
				stmtCheck.setInt(1, userid);
				ResultSet rs=stmtCheck.executeQuery();
				while(rs.next())
				{
					Payments payment=new Payments(rs.getInt("id"),rs.getInt("userid"),rs.getDouble("amount"),rs.getString("type"));
					if(payment.getAmount()>0)
						flag =1;
					else 
						flag = 0;
				}
				return flag;
	}
	public int addFine(Payments payment) throws SQLException
	{
		ResultSet rs=stmtCount.executeQuery();
		int count,flag = 0;
		while(rs.next())
		{
		count =rs.getInt(1);
		count ++;
		this.stmtUpdate.setInt(1, count);
		this.stmtUpdate.setInt(2, payment.getUserid());
		this.stmtUpdate.setDouble(3, payment.getAmount());
		this.stmtUpdate.setString(4,payment.getType());
		this.stmtUpdate.setString(5, (payment.getTransaction_time().toString()));
		this.stmtUpdate.setString(6, (payment.getNextpayment_duedate().toString()));
		flag=this.stmtUpdate.executeUpdate();
		
		}
		if(flag==1)
		{
			return flag;
		}
		else
		{
			System.out.println("Error in payment of fine");
			return flag;
		}
		
	}
	
	public List<Payments> feeReport() throws SQLException
	{
		 List<Payments> paymentlist=new ArrayList<Payments>();
		
		ResultSet rs=stmtShow.executeQuery();

		while(rs.next())
		{
			System.out.println(rs.getString(4));
			Payments pay=new Payments(rs.getInt("id"),rs.getInt("userid"),rs.getDouble("amount"),rs.getString("type"),rs.getString("transaction_time"),rs.getString("nextpayment_duedate"));
			paymentlist.add(pay);
		}
		return paymentlist;
		
	}
	
	public void close() 
	{
		try {
			this.stmtadd.close();
			this.stmtCheck.close();
			this.stmtCount.close();
			this.stmtUpdate.close();
			this.connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
