package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import pojo.Date;
import pojo.IssueRecord;
import pojo.Payments;
import utils.DBUtils;

public class IssueRecordDao 
{
	private Connection connection;
	private PreparedStatement stmtadd;
	private PreparedStatement stmtCount;
	private PreparedStatement stmtback;
	private PreparedStatement stmtdate;
	private PreparedStatement stmtCountPayment;
	public IssueRecordDao()
		{
			try 
			{
				this.connection=DBUtils.getConnection();
				this.stmtCount=this.connection.prepareStatement("select count(*) from issuerecord");
				this.stmtadd=this.connection.prepareStatement("insert into issuerecord values (?,?,?,?,?,?,?)");
				this.stmtback=this.connection.prepareStatement("update issuerecord set return_date=? , fine_amount=? where copyid=? and memberid=?");
				this.stmtdate=this.connection.prepareStatement("select return_duedate from issuerecord where copyid=?");
				this.stmtCountPayment=this.connection.prepareStatement("select count(*) from payments");
			}
			catch (ClassNotFoundException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	
	public int issueBookCopy(IssueRecord record) throws SQLException
	{
		ResultSet rs=stmtCount.executeQuery();
		int count,flag = 0;
		while(rs.next())
		{
		count =rs.getInt(1);
		count ++;
		
		
		Calendar myCal = Calendar.getInstance();
		myCal.set(Calendar.YEAR, record.getReturn_duedate().getYear());
		myCal.set(Calendar.MONTH, (record.getReturn_duedate().getMonth()-1));
		myCal.set(Calendar.DAY_OF_MONTH, record.getReturn_duedate().getDay());
		java.util.Date test = myCal.getTime();
//		test.setDate(10);
//		test.setDate(record.getReturn_duedate().getMonth());
//		test.setDate(record.getReturn_duedate().getYear());
//   	System.out.println(record.getReturn_duedate().getDay()+" "+record.getReturn_duedate().getMonth()+" "+record.getReturn_duedate().getYear());
		System.out.println(" "+new java.sql.Date(test.getTime()));
		
		
		this.stmtadd.setInt(1, count);
		this.stmtadd.setInt(2, record.getCopyid());
		this.stmtadd.setInt(3, record.getMemberid());
		this.stmtadd.setString(4,record.getIssue_date().toString());
		//this.stmtadd.setString(5, record.getReturn_duedate().toString());
		this.stmtadd.setDate(5, new java.sql.Date( test.getTime()));
		this.stmtadd.setString(6, record.getReturn_date().toString());
		this.stmtadd.setDouble(7, record.getFine_amount());
		flag=this.stmtadd.executeUpdate();
		}
		if(flag==1)
		{
			System.out.println("Book issue record added Successfully");
			return flag;
		}
		else
		{
			System.out.println("cannot be issued");
			return flag;
		}
	
	}
	
	public int returnBookCopy(IssueRecord record) throws SQLException
	{
		PaymentDao paydao=new PaymentDao();
		DateTest datetest = new DateTest();
		int flag = 0,count=0;
		double fine=0;
		this.stmtdate.setInt(1,record.getCopyid());
	
		ResultSet rs=stmtdate.executeQuery();
		ResultSet rs1=stmtCountPayment.executeQuery();
		while(rs.next() && rs1.next())
		{
			count =rs1.getInt(1);
			count ++;
			java.util.Date ret_due_date = rs.getDate("return_duedate");
			
			@SuppressWarnings("deprecation")
			Date ret_due=new Date(ret_due_date.getDate(),(ret_due_date.getMonth()+1),(1900+ret_due_date.getYear()));			
			fine=(datetest.getDateDiff(record.getReturn_date(), ret_due))*5;
			this.stmtback.setString(1, record.getReturn_date().toString());
			this.stmtback.setDouble(2, fine);
			this.stmtback.setInt(3,record.getCopyid()); 
			this.stmtback.setInt(4,
			record.getMemberid());
			
		flag=this.stmtback.executeUpdate();
		}
		System.out.println("Applicable fine : "+fine);
		if(fine>0)
		{
			Date date=new Date(0,0,0);
			Payments payment =new Payments(count,record.getMemberid(),fine,"fine",record.getReturn_date(),date);
			paydao.addFine(payment);
		}
		return flag;
	}
	
	public void close() 
	{
		try {
			this.stmtadd.close();
			this.stmtback.close();
			this.stmtCount.close();
			this.stmtCountPayment.close();
			this.stmtdate.close();
			this.connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
