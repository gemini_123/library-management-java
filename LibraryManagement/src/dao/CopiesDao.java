package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojo.Copies;
import utils.DBUtils;

public class CopiesDao 
{

	private Connection connection;
	private PreparedStatement stmtadd;
	private PreparedStatement stmtCount;
	private PreparedStatement stmtSearch;
	private PreparedStatement stmtEdit;
	private PreparedStatement stmtChange;
	private PreparedStatement stmtShowcopy;
	
	public CopiesDao()
	{
		try 
		{
			
			this.connection=DBUtils.getConnection();
			this.stmtCount=this.connection.prepareStatement("select count(*) from copies");
			this.stmtadd=this.connection.prepareStatement("insert into copies values (?,?,?,?)");
			this.stmtSearch=this.connection.prepareStatement("select * from copies where status='available' and id=?");
			this.stmtEdit=this.connection.prepareStatement("update copies set rack=? where id=?");
			this.stmtChange=this.connection.prepareStatement("update copies set status=? where id=?");
			this.stmtShowcopy=this.connection.prepareStatement("select * from books");
			
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void addCopy(Copies copy) throws SQLException
	{
		ResultSet rs=stmtCount.executeQuery();
		int count;
		while(rs.next())
		{
		count =rs.getInt(1);
		count ++;
		this.stmtadd.setInt(1, count);
		this.stmtadd.setInt(2, copy.getBookid());
		this.stmtadd.setString(3, copy.getRack());
		this.stmtadd.setString(4,"available");
		this.stmtadd.executeUpdate();
		System.out.println("Copy Added Successfully");
		}
	
	}
	//change rack
	public int changeRack(int copyid , String rack) throws SQLException
	{
		this.stmtEdit.setInt(2, copyid);
		this.stmtEdit.setString(1, rack);
		System.out.println("copyid "+copyid+" rack"+rack);
		return this.stmtEdit.executeUpdate();
	}

	// Change status
	public int changeStatus(int copyid) throws SQLException
	{
		this.stmtChange.setString(1, "not");
		this.stmtChange.setInt(2, copyid);
		return this.stmtChange.executeUpdate();
	}
	// change status after return
	public int changeStatusAfterReturn(int copyid) throws SQLException
	{
		this.stmtChange.setString(1, "available");
		this.stmtChange.setInt(2, copyid);
		return this.stmtChange.executeUpdate();
	}
	//show copies
	public void showCopies() throws SQLException
	{
		ResultSet rs=stmtShowcopy.executeQuery();

		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" , "+rs.getString(2)+" , "+rs.getString(3)+" , "+rs.getString(4)+ " , "+rs.getDouble(5)+" , "+rs.getString(6));
			System.out.println();
		}
	}
	//check copy availability
	 public int checkAvailability(int copyid) throws SQLException
	 {
		int flag=0;
		List<Copies> copylist=new ArrayList<Copies>();
			stmtSearch.setInt(1, copyid);
			ResultSet rs=stmtSearch.executeQuery();
			while(rs.next())
			{
				Copies copy=new Copies(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4));
				copylist.add(copy);
				flag ++;
			}
			for(Copies c:copylist)
			{
				System.out.println(c);
			}
		return flag;	
	 }
	 public void close() throws SQLException 
		{
		 
			this.stmtChange.close();
			 this.stmtShowcopy.close();
			this.stmtadd.close();
			this.stmtCount.close();
			this.stmtEdit.close();
			this.stmtSearch.close();
			this.connection.close();
		}
}
