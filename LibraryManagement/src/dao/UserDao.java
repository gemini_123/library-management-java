package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pojo.Users;
import test.Common;
import utils.DBUtils;

public class UserDao implements Closeable
{
	private Connection connection;
	private PreparedStatement stmtSignIn;
	private PreparedStatement stmtSignUp;
	private PreparedStatement stmtCount;
	private PreparedStatement stmtUpdate;
	private PreparedStatement stmtEdit;
	
	public UserDao() 
	{
		try
		{
			this.connection=DBUtils.getConnection();
			this.stmtSignIn=this.connection.prepareStatement("select * from users");
			this.stmtCount=this.connection.prepareStatement("select count(*) from users");
			this.stmtSignUp=this.connection.prepareStatement("insert into users values (?,?,?,?,?,?)");
			this.stmtUpdate=this.connection.prepareStatement("update users set passwd=? where email=?");
			this.stmtEdit=this.connection.prepareStatement("update users set name?,phone=?,role=? where email=? and passwd=?");
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	//for sign in - email , password and role
	
	public void signIn(String email,String password) throws Exception
	{
		ResultSet rs=stmtSignIn.executeQuery();
		String role="";int success = 0;
		while(rs.next())
		{
			if(email.equals(rs.getString("email")) && password.equals(rs.getString("passwd")))
				{
				System.out.println("welcome "+rs.getString("name"));
				role=rs.getString("role");	
				if(role.equals("owner"))
					Common.ownermenu();
				if(role.equals("librarian"))
					Common.librarianmenu();
				if(role.equals("member"))
					Common.membermenu();
				success=1;
				}
			else
				success=0;
						
				}
		if(success==0)
			System.out.println("Invalid username or password");
		
		}
		
	 //for sign up 
	public void signUp(Users users) throws Exception
	{		
			ResultSet rs=stmtCount.executeQuery();
			int count;
			while(rs.next())
			{
			count =rs.getInt(1);
			count ++;
			this.stmtSignUp.setInt(1, count);
			this.stmtSignUp.setString(2, users.getName());
			this.stmtSignUp.setString(3, users.getEmail());
			this.stmtSignUp.setString(4,users.getPhone());
			this.stmtSignUp.setString(5,users.getPasswd());
			this.stmtSignUp.setString(6, users.getRole());
			this.stmtSignUp.executeUpdate();
			System.out.println("Sign Up Successfull");
			}
		
	}
	
	//update password
	public int  updatePassword(String email,String password) throws SQLException
	{
		this.stmtUpdate.setString(2,email);
		this.stmtUpdate.setString(1, password);
		return this.stmtUpdate.executeUpdate();
		
		
	}
	//edit Profile
	public int editProfile(String name ,String phone ,String role,String email , String Password) throws Exception
	{
		this.stmtEdit.setString(1, name);
		this.stmtEdit.setString(2, phone);
		this.stmtEdit.setString(3, role);
		this.stmtEdit.setString(4, email);
		this.stmtEdit.setString(5, Password);
		return this.stmtEdit.executeUpdate();
	}
	//read method
	/*public List<Users> getUser()
	{
		String sql="select * from users";
		List<Users> userlist=new ArrayList<Users>();
		ResultSet rs;
		try {
			rs = this.statement.executeQuery(sql);
			while(rs.next())
			{
				Users user=new Users(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
				userlist.add(user);
			}	
		}	
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return userlist;
		
	}*/
	public void close() 
	{
		try {
			this.stmtSignIn.close();
			this.stmtCount.close();
			this.stmtEdit.close();
			this.stmtSignUp.close();
			this.stmtUpdate.close();
			this.connection.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
