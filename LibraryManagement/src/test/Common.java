package test;



import java.time.LocalDate;
import java.util.Calendar;
import java.util.Scanner;

import dao.BookDao;
import dao.CopiesDao;
import dao.IssueRecordDao;
import dao.PaymentDao;
import dao.UserDao;
import pojo.Book;
import pojo.Copies;
import pojo.Date;
import pojo.IssueRecord;
import pojo.Payments;
import pojo.Users;

public class Common 
{
	static Scanner sc=new Scanner(System.in);
	static int choice=0,id=0,bookid=0,copyid=0, memberid,flag;
	static BookDao bookdao=new BookDao();
	static UserDao userdao=new UserDao();
	static CopiesDao copydao=new CopiesDao();
	static PaymentDao paydao=new PaymentDao();
	static IssueRecordDao issue=new IssueRecordDao();
	static String name,phone,email,password,role,author,subject,isbn,rack;
	static double fine_amount,price,date_diff;

	static Calendar c = Calendar.getInstance();
	static LocalDate l=LocalDate.now(); 
	static Date curdate=new Date((int)(l.getDayOfMonth()),(int)(l.getMonthValue()),(int)(l.getYear()));
	static LocalDate next=(l.plusDays(30));
	static LocalDate next_book=l.plusDays(5);
	
	static Date nextduedate=new Date(next.getDayOfMonth(),next.getMonthValue(),next.getYear());
	static Date nextduedate_book=new Date(next_book.getDayOfMonth(),next_book.getMonthValue(),next_book.getYear());
    static Date issue_date, return_duedate, return_date;
	public static void ownermenu() throws Exception
	{
		System.out.println("enter choice");
		do
		{
			System.out.println(" 1. Appoint Librarian \n 2.Edit Profile");
			System.out.println(" 3.Edit Password \n 4. Fees Report & Fine Report");
			System.out.println(" 5. Book & Copies report \n 6.Book Availability \n 0.Sign Out");
			choice=sc.nextInt();
		switch(choice)
		{
		case 1:
			System.out.println("Appoint libraraian");
			Users user = new Users();
			System.out.println("enter name ");
			sc.nextLine();
			user.setName(sc.nextLine());
			System.out.println("Enter email ");
			sc.nextLine();
			user.setEmail(sc.nextLine());
			System.out.println("Enter Phone Number");
			user.setPhone(sc.nextLine());
			System.out.println("Enter Password");
			user.setPasswd(sc.nextLine());
			System.out.println("Enter Role");
			user.setRole(sc.nextLine());
			userdao.signUp(user);
			break;
		case 2:
			System.out.println("Enter Email");
			email=sc.nextLine();
			System.out.println("Enter Password");
			password=sc.nextLine();
			System.out.println("Enter New name");
			name=sc.nextLine();
			System.out.println("Enter updated role");
			role=sc.nextLine();
			System.out.println("Enter phone");
			phone=sc.nextLine();
			flag=userdao.editProfile(name, phone, role, email, password);
			if(flag==1)
				System.out.println("Profile changed successfully");
			else
				System.out.println("Enter correct details");
			break;
		case 3:
			System.out.println("enter email ID");
			sc.nextLine();
			email=sc.nextLine();
			System.out.println("enter new Password");
			password=sc.nextLine();
			flag=userdao.updatePassword(email, password);
			if(flag==1)
				System.out.println("Password change successful");
			else
				System.out.println("Please enter correct details");
			break;
		case 4:
			//fee report
			System.out.println("Fees & fine Report: ");
			System.out.println(paydao.feeReport());
			break;
		case 5:
			//book categoris
			System.out.println("Book Report ");
			bookdao.bookReport();
			break;
		case 6:
			// Book availability status
			System.out.println("Book availability status");
			bookdao.bookAvailReport();
		}
		}
		while(choice!=0);
		}
		
	
	public static void librarianmenu() throws Exception
	{
		System.out.println("Enter Choice");
		do
		{
			System.out.println(" 1. Edit password \n 2.Edit profile ");
			System.out.println(" 3.Return copy \n 4. Issue Copy \n 5.Find Book \n 6.Edit Book");
			System.out.println(" 7.Add new book \n 8.Check Availability \n 9.Add new copy");
			System.out.println(" 10.Change Rack \n 11.Add new Member \n 12.Take payment");
			choice=sc.nextInt();
		switch(choice)
		{
		case 1:
			System.out.println("enter email ID");
			sc.nextLine();
			email=sc.nextLine();
			System.out.println("enter new Password");
			password=sc.nextLine();
			flag=userdao.updatePassword(email, password);
			if(flag==1)
				System.out.println("Password change successful");
			else
				System.out.println("Please enter correct details");
			break;
		case 2:
			System.out.println("Enter Email");
			email=sc.nextLine();
			System.out.println("Enter Password");
			password=sc.nextLine();
			System.out.println("Enter New name");
			name=sc.nextLine();
			System.out.println("Enter updated role");
			role=sc.nextLine();
			System.out.println("Enter phone");
			phone=sc.nextLine();
			flag=userdao.editProfile(name, phone, role, email, password);
			if(flag==1)
				System.out.println("Profile changed successfully");
			else
				System.out.println("Enter correct details");
			break;
		case 3:
			System.out.println("enter copy id to return");
			copyid=sc.nextInt();
			System.out.println("enter member id");
			memberid=sc.nextInt();
			return_date=curdate;
			IssueRecord recordreturn=new IssueRecord(id, copyid, memberid, issue_date, return_duedate, return_date, fine_amount);
			//issue.returnBookCopy(recordreturn);
			if(issue.returnBookCopy(recordreturn)>0)
			{
				copydao.changeStatusAfterReturn(copyid);
				System.out.println("book returned");
			}
		else
			System.out.println("error");
			break;
		case 4:
			issue_date=curdate;
			return_duedate=nextduedate_book;
			System.out.println("enter copy id");
			copyid=sc.nextInt();
			System.out.println("enter member id");
			memberid=sc.nextInt();
			return_date=new Date(0,0,0);
			IssueRecord record=new IssueRecord(id, copyid, memberid, issue_date, return_duedate, return_date, fine_amount);
			
			if(copydao.checkAvailability(copyid)>0 && paydao.isPaid(memberid)==1)
				{
					issue.issueBookCopy(record);
					copydao.changeStatus(copyid);
				}
			else
				if(copydao.checkAvailability(copyid)<=0)
				{
					System.out.println("sorry copy is not available");
				}
				else 
					if(paydao.isPaid(memberid)!=1)
					{
						System.out.println("Member is not paid member");
					}
			break;
		case 5:
			String str;
			System.out.println("Enter name  of book");
			sc.nextLine();
			str=sc.nextLine();
			System.out.println(bookdao.findBook(str));
			break;
			
		case 6:
			System.out.println("Enter book ID");
			id=sc.nextInt();
			System.out.println("Enter Book name");
			sc.nextLine();
			name=sc.nextLine();
			System.out.println("Enter author");
			author=sc.nextLine();
			System.out.println("Enter subject");
			subject=sc.nextLine();
			System.out.println("Enter updated price");
			price=sc.nextDouble();
			System.out.println("Enter ISBN");
			sc.nextLine();
			isbn=sc.nextLine();
			flag=bookdao.editBook(id,name, author, subject,price, isbn);
			if(flag==1)
				System.out.println("Book changed successfully");
			else
				System.out.println("Enter correct details");
			break;
			
		case 7:
			Book book = new Book();
			System.out.println("enter name ");
			sc.nextLine();
			book.setName(sc.nextLine());
			System.out.println("Enter Author ");
			book.setAuthor(sc.nextLine());
			System.out.println("Enter Subject");
			book.setSubject(sc.nextLine());
			System.out.println("Enter Price");
			book.setPrice(sc.nextDouble());
			System.out.println("Enter ISBN");
			sc.nextLine();
			book.setIsbn(sc.nextLine());
			bookdao.addBook(book);
			break;
		case 8:
			System.out.println("Enter copy id to check availability : ");
			copyid=sc.nextInt();
			flag=copydao.checkAvailability(copyid);
			if(flag==1)
				{
					copydao.checkAvailability(copyid);
				}
				
			else
				System.out.println("Sorry not available");
			//System.out.println(copydao.checkAvailability(copyid)+" copies available");
			break;
		case 9: 
			Copies copy=new Copies();
			System.out.println("Enter book id");
			copy.setBookid(sc.nextInt());
			System.out.println("Enter Rack");
			sc.nextLine();
			copy.setRack(sc.nextLine());
			copydao.addCopy(copy);
			break;
		case 10:
			System.out.println("Enter copy ID for which rack needs to be changed");
			copyid=sc.nextInt();
			System.out.println("Enter new rack name");
			sc.nextLine();
			rack=sc.nextLine();
			flag=copydao.changeRack(copyid, rack);
			if(flag==1)
				System.out.println("Rack changed successfully");
			else
				System.out.println("Enter correct details");
			break;
			
		case 11:
			Users user = new Users();
			System.out.println("enter name ");
			sc.nextLine();
			user.setName(sc.nextLine());
			System.out.println("Enter email ");
			user.setEmail(sc.nextLine());
			System.out.println("Enter Phone Number");
			user.setPhone(sc.nextLine());
			System.out.println("Enter Password");
			user.setPasswd(sc.nextLine());
			System.out.println("Enter Role");
			user.setRole(sc.nextLine());
			userdao.signUp(user);
			break;
		case 12:
			Payments pay=new Payments();
			System.out.println("enter member ID ");
			sc.nextLine();
			pay.setUserid(sc.nextInt());
			System.out.println("Enter Amount ");
			pay.setAmount(sc.nextDouble());
			System.out.println("Enter Type : membership / fine");
			sc.nextLine();
			pay.setType(sc.nextLine());
			//System.out.println("cur date ");
			pay.setTransaction_time(curdate);
			pay.setNextpayment_duedate(nextduedate);
			paydao.takePAyment(pay);
			break;
		}
		}
		while(choice!=0);
	}
	public static void membermenu() throws Exception
	{
		do
		{
		System.out.println(" 1.Edit Profile");
		System.out.println(" 2.Find Book \n 3.Check availability \n 0. Sign out");
		choice=sc.nextInt();
		switch(choice)
		{
		case 1:
			System.out.println("Enter Email");
			email=sc.nextLine();
			System.out.println("Enter Password");
			password=sc.nextLine();
			System.out.println("Enter New name");
			name=sc.nextLine();
			System.out.println("Enter updated role");
			role=sc.nextLine();
			System.out.println("Enter phone");
			phone=sc.nextLine();
			flag=userdao.editProfile(name, phone, role, email, password);
			if(flag==1)
				System.out.println("Profile changed successfully");
			else
				System.out.println("Enter correct details");
			break;
		case 2:
			String str;
			System.out.println("Enter name  of book");
			sc.nextLine();
			str=sc.nextLine();
			System.out.println(bookdao.findBook(str));
			break;
		case 3:
			System.out.println("Copy list, select available copy id");
			copydao.showCopies();
			System.out.println("Enter copy id to check availability : ");
			copyid=sc.nextInt();
			flag=copydao.checkAvailability(copyid);
			if(flag==1)
				{
					copydao.checkAvailability(copyid);
				}
				
			else
				System.out.println("Sorry not available");
			break;
		}
		}
		while(choice!=0);
	}
}
