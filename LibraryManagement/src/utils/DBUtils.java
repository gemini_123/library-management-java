package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DBUtils 
{
	static Properties p;
	static 
	{
		p=new Properties();
		try 
		{
			FileInputStream inputstream=new FileInputStream("config.properties");
			p.load(inputstream);
			Class.forName(p.getProperty("DRIVER"));
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static Connection getConnection() throws ClassNotFoundException, SQLException, IOException
	{
		
		//establish connection with db
		return DriverManager.getConnection(p.getProperty("URL"),p.getProperty("USERNAME"),p.getProperty("PASSWORD"));
		
	}

}
